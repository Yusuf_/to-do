var express = require('express');
var router = express.Router();
var moviesCtrl = require('../controllers/movies');
var showsCtrl = require('../controllers/shows');

var otherCtrl = require('../controllers/others');

/* Tasks pages */
router.get('/', moviesCtrl.tasklist);
router.get('/movies', moviesCtrl.taskInfo);
router.get('/movies/new', moviesCtrl.addTask);

/* Other pages */
router.get('/about', otherCtrl.about);

module.exports = router;

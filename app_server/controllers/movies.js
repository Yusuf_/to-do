/* GET 'home' page */
module.exports.tasklist = function(req, res){
	res.render('homelist', {
		title : 'Movie Manager - Never lose track of the movies',
		pageHeader : {
			title : 'Movie Manager',
			strapline : 'Find all the latest movie you like'
		},
		movies : [{
			title : 'Prisoners',
			rating : 7.5,
			description : 'Wolverine goes crazy when his daughter is kidnapped,',
			categories : ['Thriller', 'Action', 'Drama']
		},
		{
			title : 'Ali',
			rating : 7.3,
			description : 'Life of legendary boxer played by Will Smith',
			categories : ['Documentary', 'Drama']
		}]
	});
};

/* GET 'task detail' page */
module.exports.taskInfo = function(req, res){
	res.render('movie-info', {title : 'Movie detail'});
};

/* GET 'Add task' page */
module.exports.addTask = function(req, res){
	res.render('movie-new', {title : 'Add movie'});
};

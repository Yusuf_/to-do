/**
 * Created by Y.A on 31-7-2016.
 */
var mongoose = require('mongoose');

var movieSchema = new mongoose.Schema({
    title : {type : String, required : true},
    rating : {type : Number, "default" : 1, min : 1, max : 10, required : true},
    description : String,
    categories : [String]
});

mongoose.model('Movies', movieSchema);
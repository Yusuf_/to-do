/**
 * Created by Y.A on 31-7-2016.
 */
var mongoose = require('mongoose');

var dbURI = 'mongodb://localhost/moviemanager';

if(process.env.NODE_ENV === 'production'){
    dbURI = process.env.MONGOLAB_URI;
}

mongoose.connect(dbURI);

mongoose.connection.on('connected', function () {
    console.log('Mongoose connected to ' + dbURI);
});

mongoose.connection.on('disconnected', function () {
    console.log('Mongoose disconnected');
});

mongoose.connection.on('err', function (err) {
    console.log('Mongoose connection error: ' + err);
});

require('./movies');